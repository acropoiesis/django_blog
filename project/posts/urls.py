from django.conf.urls import url, include
from posts import views

urlpatterns = [
    url(r'^posts/$', views.PostListView.as_view(), name="post_list"),
    url(r'^posts/new/$', views.PostCreateView.as_view(), name='create_post'),
    url(r'^posts/(?P<slug>[-\w]+)/$', views.PostDetailView.as_view(), name='detail_post'),
    url(r'^posts/(?P<slug>[-\w]+)/edit/$', views.PostUpdateView.as_view(), name='edit_post'),
    url(r'^posts/(?P<slug>[-\w]+)/delete/$', views.PostDeleteView.as_view(), name='delete_post'),
]
